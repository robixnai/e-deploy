package br.com.rmoreira.cityscore.network.retrofit

import br.com.rmoreira.cityscore.CityScoreApplication
import br.com.rmoreira.cityscore.network.apis.CityApi
import br.com.rmoreira.cityscore.network.apis.MapsGeocodeApi
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by robsonmoreira on 15/10/17.
 */
class Client {

    companion object {
        fun getCityClient(): Retrofit? {
            val cache = Cache(CityScoreApplication.getFileCacheDir(), (10 * 1024 * 1024).toLong())

            val client = OkHttpClient.Builder()
                    .readTimeout(20, TimeUnit.SECONDS)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .cache(cache)
                    .addInterceptor { chain ->
                        var request = chain.request()
                        if (CityScoreApplication.isNetworkAvailable()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build()
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                        }
                        chain.proceed(request)
                    }
                    .build()

            return Retrofit.Builder().baseUrl(CityApi.BASE).addConverterFactory(GsonConverterFactory.create()).client(client).build()
        }

        fun getMapsGeocodeClient(): Retrofit? {
            val cache = Cache(CityScoreApplication.getFileCacheDir(), (10 * 1024 * 1024).toLong())

            val client = OkHttpClient.Builder()
                    .readTimeout(20, TimeUnit.SECONDS)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .cache(cache)
                    .addInterceptor { chain ->
                        var request = chain.request()
                        if (CityScoreApplication.isNetworkAvailable()) {
                            request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build()
                        } else {
                            request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build()
                        }
                        chain.proceed(request)
                    }
                    .build()

            return Retrofit.Builder().baseUrl(MapsGeocodeApi.BASE).addConverterFactory(GsonConverterFactory.create()).client(client).build()
        }
    }

}