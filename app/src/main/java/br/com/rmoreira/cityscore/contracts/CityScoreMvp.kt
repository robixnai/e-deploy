package br.com.rmoreira.cityscore.contracts

import android.os.Bundle
import br.com.rmoreira.cityscore.objects.CityGeocodeObject
import br.com.rmoreira.cityscore.objects.CityObject
import br.com.rmoreira.cityscore.objects.LocationObject

/**
 * Created by robsonmoreira on 14/10/17.
 */
interface CityScoreMvp {

    interface CitiesListModelImpl {
        fun getCitiesList(cityName: String, country: String)
        fun responseCitiesList(success: ArrayList<CityObject>?, failure: String?)
    }
    interface CitiesListViewImpl {
        fun updateRecyclerView()
        fun showProgressBar(visibility: Int)
        fun showNoData()
        fun showLackConnection()
    }
    interface CitiesListPresenterImpl {
        val CITIES_KEY: String get() = "cities"

        fun getCitiesList(savedInstanceState: Bundle?, cityName: String, country: String)
        fun updateCitiesList(cities: ArrayList<CityObject>?, error: String?)
        fun getCities(): ArrayList<CityObject>
    }

    interface CityDetailModelImpl {
        fun getCityScore(city: CityObject)
        fun getCityGeocode()
        fun responseCityScore(success: String?, failure: String?)
        fun responseCityGeocode(success: LocationObject?, failure: String?)
    }
    interface CityDetailViewImpl {
        fun updateView()
        fun showProgressBar(visibility: Int)
    }
    interface CityDetailPresenterImpl {
        fun getCityScore(extras: Bundle)
        fun updateDataView(result: Any?, error: String?)
        fun getCityName(): String
        fun getScore(): String
        fun getLatitude(): Double
        fun getLongitude(): Double
    }

}