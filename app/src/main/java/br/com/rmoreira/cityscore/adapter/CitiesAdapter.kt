package br.com.rmoreira.cityscore.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.rmoreira.cityscore.R
import br.com.rmoreira.cityscore.adapter.holders.CitiesViewHolder
import br.com.rmoreira.cityscore.contracts.OnItemClickListener
import br.com.rmoreira.cityscore.objects.CityObject

/**
 * Created by robsonmoreira on 14/10/17.
 */
class CitiesAdapter(private val itens: MutableList<CityObject>, private val listener: OnItemClickListener) : RecyclerView.Adapter<CitiesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesViewHolder? {
        val view = LayoutInflater.from(parent.getContext()).inflate(R.layout.city_itens_recycler_view, parent, false)
        return CitiesViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: CitiesViewHolder, position: Int) {
        val item = itens[position]
        holder.onBindViewHolder(item)
    }

    override fun getItemCount(): Int {
        return itens.size
    }

}