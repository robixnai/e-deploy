package br.com.rmoreira.cityscore.models

import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.network.retrofit.Service
import br.com.rmoreira.cityscore.network.routers.CitiesRouters
import br.com.rmoreira.cityscore.objects.CityObject

/**
 * Created by robsonmoreira on 14/10/17.
 */
class CitiesListModel(private val presenter: CityScoreMvp.CitiesListPresenterImpl) : CityScoreMvp.CitiesListModelImpl {

    private lateinit var cityName: String
    private lateinit var country: String

    override fun getCitiesList(cityName: String, country: String) {
        this.cityName = cityName
        this.country = country

        CitiesRouters(Service.getCityService()!!, this).getCities()
    }

    override fun responseCitiesList(success: ArrayList<CityObject>?, failure: String?) {
        var cities: ArrayList<CityObject>? = null
        if (success != null && failure == null) {
            cities = arrayListOf()
            for (city in success) {
                if (containsInCity(city.name) && city.country.equals(country, true)) {
                    cities.add(city)
                }
            }
        }
        presenter.updateCitiesList(cities, failure)
    }

    private fun containsInCity(value: String): Boolean {
        var constains: Boolean = false
        if (value.length >= cityName.length) {
            val substring = value.substring(0, cityName.length)
            constains = substring.equals(cityName, true)
        }
        return constains
    }

}