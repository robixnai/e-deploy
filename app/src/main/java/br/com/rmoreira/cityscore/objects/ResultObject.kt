package br.com.rmoreira.cityscore.objects

import com.google.gson.annotations.SerializedName

/**
 * Created by robsonmoreira on 15/10/17.
 */
class ResultObject(@SerializedName("geometry") var geometry: GeometryObject)