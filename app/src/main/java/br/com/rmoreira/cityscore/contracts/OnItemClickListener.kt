package br.com.rmoreira.cityscore.contracts

import android.view.View
import br.com.rmoreira.cityscore.objects.CityObject

/**
 * Created by robsonmoreira on 14/10/17.
 */
interface OnItemClickListener {

    fun onItemClick(view: View, city: CityObject)

}