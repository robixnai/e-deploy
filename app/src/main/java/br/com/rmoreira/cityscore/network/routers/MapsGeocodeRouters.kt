package br.com.rmoreira.cityscore.network.routers

import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.network.apis.MapsGeocodeApi
import br.com.rmoreira.cityscore.objects.CityGeocodeObject
import br.com.rmoreira.cityscore.objects.LocationObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by robsonmoreira on 15/10/17.
 */
class MapsGeocodeRouters(private val api: MapsGeocodeApi, private val model: CityScoreMvp.CityDetailModelImpl) {

    fun getLocation(cityName: String) {
        api.getLocation(cityName).enqueue(object : Callback<CityGeocodeObject> {
            override fun onResponse(call: Call<CityGeocodeObject>, response: Response<CityGeocodeObject>) {
                if (response.isSuccessful) {
                    val results = response.body()!!.results
                    var location: LocationObject? = null
                    if (!results.isEmpty()) {
                        location = results.get(0).geometry.location
                    }
                    model.responseCityGeocode(location, null)
                } else {
                    model.responseCityGeocode(null, response.errorBody()!!.string())
                }
            }

            override fun onFailure(call: Call<CityGeocodeObject>, throwable: Throwable) {
                model.responseCityGeocode(null, throwable.message)
            }
        })
    }

}