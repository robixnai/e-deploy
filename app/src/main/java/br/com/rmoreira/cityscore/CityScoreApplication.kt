package br.com.rmoreira.cityscore

import android.app.Application
import android.util.Log
import br.com.rmoreira.cityscore.utils.AppUtil
import java.io.File

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CityScoreApplication : Application() {

    private val TAG = "CityScoreApplication"

    override fun onCreate() {
        super.onCreate()

        appInstance = this
    }

    companion object {
        private var appInstance: CityScoreApplication? = null

        fun getInstance(): CityScoreApplication {
            if (appInstance == null) {
                throw IllegalStateException("Configure the Application class in AndroidManifest.xml")
            }
            return appInstance!!
        }

        fun isNetworkAvailable(): Boolean {
            return AppUtil.isNetworkAvailable(CityScoreApplication.getInstance().applicationContext)
        }

        fun getFileCacheDir(): File {
            return CityScoreApplication.getInstance().getCacheDir()
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        Log.d(TAG, "CityScoreApplication.onTerminate()")
    }

}