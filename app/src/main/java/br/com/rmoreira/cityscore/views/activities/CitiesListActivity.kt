package br.com.rmoreira.cityscore.views.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import br.com.rmoreira.cityscore.R
import br.com.rmoreira.cityscore.adapter.CitiesAdapter
import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.contracts.OnItemClickListener
import br.com.rmoreira.cityscore.objects.CityObject
import br.com.rmoreira.cityscore.presenters.CitiesListPresenter
import kotlinx.android.synthetic.main.activity_cities_list.*

class CitiesListActivity : AppCompatActivity(), CityScoreMvp.CitiesListViewImpl, OnItemClickListener {

    companion object {
        val SEARCH = "search"
    }

    private lateinit var presenter: CitiesListPresenter
    private lateinit var mAdapter: CitiesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cities_list)

        val charArray = intent.extras.getStringArray(SEARCH)

        presenter = CitiesListPresenter(this)
        presenter.getCitiesList(savedInstanceState, charArray[0], charArray[1])
    }

    override fun onStart() {
        super.onStart()

        bindAdapter()
    }

    public override fun onSaveInstanceState(outState: Bundle?) {
        outState!!.putParcelableArrayList(presenter.CITIES_KEY, presenter.getCities())
        super.onSaveInstanceState(outState)
    }

    override fun updateRecyclerView() {
        mAdapter.notifyDataSetChanged()
    }

    override fun showProgressBar(visibility: Int) {
        progressBarCitiesList.visibility = visibility
    }

    override fun showNoData() {
        includeNoData.visibility = View.VISIBLE
    }

    override fun showLackConnection() {
        includeLackConnection.visibility = View.VISIBLE
    }

    override fun onItemClick(view: View, city: CityObject) {
        val intent = Intent(this@CitiesListActivity, CityDetailActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(CityDetailActivity.CITY, city)
        startActivity(intent)
    }

    private fun bindAdapter() {
        with(recyclerViewCities) {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@CitiesListActivity)
            mAdapter = CitiesAdapter(presenter.getCities(), this@CitiesListActivity)
            adapter = mAdapter
        }
    }

}
