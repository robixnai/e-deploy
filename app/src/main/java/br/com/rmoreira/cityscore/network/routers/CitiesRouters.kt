package br.com.rmoreira.cityscore.network.routers

import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.network.apis.CityApi
import br.com.rmoreira.cityscore.objects.CityObject
import br.com.rmoreira.cityscore.utils.AppUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CitiesRouters(private val api: CityApi, private val model: CityScoreMvp.CitiesListModelImpl) {

    fun getCities() {
        api.getCities().enqueue(object : Callback<ArrayList<CityObject>> {
            override fun onResponse(call: Call<ArrayList<CityObject>>, response: Response<ArrayList<CityObject>>) {
                if (response.isSuccessful) {
                    model.responseCitiesList(response.body(), null)
                } else {
                    model.responseCitiesList(null, AppUtil.getErrorInResponse(response.code()))
                }
            }

            override fun onFailure(call: Call<ArrayList<CityObject>>, throwable: Throwable) {
                model.responseCitiesList(null, throwable.message)
            }
        })
    }

}