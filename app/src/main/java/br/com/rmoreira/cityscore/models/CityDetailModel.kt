package br.com.rmoreira.cityscore.models

import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.network.retrofit.Service
import br.com.rmoreira.cityscore.network.routers.CityRouters
import br.com.rmoreira.cityscore.network.routers.MapsGeocodeRouters
import br.com.rmoreira.cityscore.objects.CityGeocodeObject
import br.com.rmoreira.cityscore.objects.CityObject
import br.com.rmoreira.cityscore.objects.LocationObject

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CityDetailModel(private val presenter: CityScoreMvp.CityDetailPresenterImpl) : CityScoreMvp.CityDetailModelImpl {

    private lateinit var cityName: String

    override fun getCityScore(city: CityObject) {
        cityName = city.name

        CityRouters(Service.getCityService()!!, this).cityScore(city)
    }

    override fun getCityGeocode() {
        MapsGeocodeRouters(Service.getMapsGeocodeService()!!, this).getLocation(cityName)
    }

    override fun responseCityScore(success: String?, failure: String?) {
        presenter.updateDataView(success, failure)
    }

    override fun responseCityGeocode(success: LocationObject?, failure: String?) {
        presenter.updateDataView(success, failure)
    }

}