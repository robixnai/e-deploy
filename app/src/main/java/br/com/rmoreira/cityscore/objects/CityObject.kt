package br.com.rmoreira.cityscore.objects

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CityObject(@SerializedName("Nome") var name: String, @SerializedName("Estado") var country: String) : Parcelable {

    constructor(source: Parcel) : this(source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        dest.writeString(name)
        dest.writeString(country)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CityObject> = object : Parcelable.Creator<CityObject> {
            override fun createFromParcel(source: Parcel): CityObject = CityObject(source)
            override fun newArray(size: Int): Array<CityObject?> = arrayOfNulls(size)
        }
    }
}