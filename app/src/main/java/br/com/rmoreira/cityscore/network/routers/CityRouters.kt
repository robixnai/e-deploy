package br.com.rmoreira.cityscore.network.routers

import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.network.apis.CityApi
import br.com.rmoreira.cityscore.objects.CityObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CityRouters(private val api: CityApi, private val model: CityScoreMvp.CityDetailModelImpl) {

    fun cityScore(city: CityObject) {
        api.cityScore(city).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                if (response.isSuccessful) {
                    model.responseCityScore(response.body(), null)
                } else {
                    model.responseCityScore(null, response.errorBody()!!.string())
                }
            }

            override fun onFailure(call: Call<String>, throwable: Throwable) {
                model.responseCityScore(null, throwable.message)
            }
        })
    }

}