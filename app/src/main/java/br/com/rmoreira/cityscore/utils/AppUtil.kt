package br.com.rmoreira.cityscore.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by robsonmoreira on 14/10/17.
 */
class AppUtil {

    companion object {

        fun isNetworkAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }

        fun getErrorInResponse(errorCode: Int): String {
            var errorMessage = "Connection error";
            when (errorCode) {
                401 -> errorMessage = "Invalid user or password"
            }
            return errorMessage
        }
    }

}