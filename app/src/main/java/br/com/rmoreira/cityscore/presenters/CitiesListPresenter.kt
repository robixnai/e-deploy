package br.com.rmoreira.cityscore.presenters

import android.os.Bundle
import android.view.View
import br.com.rmoreira.cityscore.CityScoreApplication
import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.models.CitiesListModel
import br.com.rmoreira.cityscore.objects.CityObject

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CitiesListPresenter(private val view: CityScoreMvp.CitiesListViewImpl) : CityScoreMvp.CitiesListPresenterImpl {

    private var model: CitiesListModel = CitiesListModel(this)
    private var cities: ArrayList<CityObject> = arrayListOf()

    override fun getCitiesList(savedInstanceState: Bundle?, cityName: String, country: String) {
        if (savedInstanceState != null) {
            this.cities = savedInstanceState.getParcelableArrayList(CITIES_KEY)
            return
        } else if (CityScoreApplication.isNetworkAvailable()) {
            view.showProgressBar(View.VISIBLE)
            model.getCitiesList(cityName, country)
        } else {
            view.showLackConnection()
        }
    }

    override fun updateCitiesList(cities: ArrayList<CityObject>?, error: String?) {
        view.showProgressBar(View.GONE)

        if (cities != null && !cities.isEmpty()) {
            this.cities.addAll(cities)
            view.updateRecyclerView()
        } else {
            view.showNoData()
        }
    }

    override fun getCities(): ArrayList<CityObject> {
        return cities
    }

}