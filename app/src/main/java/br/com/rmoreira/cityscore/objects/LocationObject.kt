package br.com.rmoreira.cityscore.objects

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by robsonmoreira on 15/10/17.
 */
class LocationObject(@SerializedName("lat") var latitude: Double, @SerializedName("lng") var longitude: Double)