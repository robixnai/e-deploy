package br.com.rmoreira.cityscore.network.retrofit

import br.com.rmoreira.cityscore.network.apis.CityApi
import br.com.rmoreira.cityscore.network.apis.MapsGeocodeApi

/**
 * Created by robsonmoreira on 15/10/17.
 */
class Service {

    companion object {
        fun getCityService(): CityApi? {
            return Client.getCityClient()?.create(CityApi::class.java)
        }

        fun getMapsGeocodeService(): MapsGeocodeApi? {
            return Client.getMapsGeocodeClient()?.create(MapsGeocodeApi::class.java)
        }
    }

}