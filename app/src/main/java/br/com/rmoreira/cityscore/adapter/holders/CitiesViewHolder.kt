package br.com.rmoreira.cityscore.adapter.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.rmoreira.cityscore.contracts.OnItemClickListener
import br.com.rmoreira.cityscore.objects.CityObject
import kotlinx.android.synthetic.main.city_itens_recycler_view.view.*

/**
 * Created by robsonmoreira on 14/10/17.
 */
class CitiesViewHolder(itemView: View, private val listener: OnItemClickListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener{

    private lateinit var city: CityObject

    fun onBindViewHolder(city: CityObject) {
        this.city = city

        itemView.textViewCity.setText(city.name)
        itemView.textViewCountry.setText(city.country)

        itemView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if (listener != null) {
            listener.onItemClick(view!!, city);
        }
    }

}