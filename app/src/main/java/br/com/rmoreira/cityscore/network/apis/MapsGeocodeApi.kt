package br.com.rmoreira.cityscore.network.apis

import br.com.rmoreira.cityscore.objects.CityGeocodeObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by robsonmoreira on 15/10/17.
 */
interface MapsGeocodeApi {

    companion object {
        val BASE: String get() = "http://maps.googleapis.com/maps/api/geocode/"
    }

    @GET("json")
    fun getLocation(@Query("address") cityName: String): Call<CityGeocodeObject>

}