package br.com.rmoreira.cityscore.views.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import br.com.rmoreira.cityscore.R
import kotlinx.android.synthetic.main.activity_search.*
import android.text.TextUtils
import android.widget.EditText

class SearchActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        buttonSearch.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.buttonSearch -> startActivity()
        }
    }

    private fun startActivity() {
        val isValid = this.verifyMandatoryFields(textInputEditTextCity, textInputEditTextCountry)
        if (isValid) {
            val intent = Intent(this, CitiesListActivity::class.java)
            intent.putExtra(CitiesListActivity.SEARCH, arrayOf(
                    textInputEditTextCity.text.toString(),
                    textInputEditTextCountry.text.toString())
            )
            startActivity(intent)
        }
    }

    private fun verifyMandatoryFields(vararg fields: EditText): Boolean {
        var isValid = true
        for (field in fields) {
            field.error = null
            if (TextUtils.isEmpty(field.text)) {
                field.error = getString(R.string.msg_mandatory)
                if (isValid) {
                    isValid = false
                }
            }
        }
        return isValid
    }

}
