package br.com.rmoreira.cityscore.presenters

import android.os.Bundle
import android.view.View
import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.models.CityDetailModel
import br.com.rmoreira.cityscore.objects.CityObject
import br.com.rmoreira.cityscore.objects.LocationObject
import br.com.rmoreira.cityscore.views.activities.CityDetailActivity

/**
 * Created by robsonmoreira on 15/10/17.
 */
class CityDetailPresenter(private val view: CityScoreMvp.CityDetailViewImpl) : CityScoreMvp.CityDetailPresenterImpl {

    private lateinit var score: String
    private lateinit var cityName: String

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private var model: CityDetailModel = CityDetailModel(this)

    override fun getCityScore(extras: Bundle) {
        val city = extras.getParcelable<CityObject>(CityDetailActivity.CITY)
        cityName = city.name

        view.showProgressBar(View.VISIBLE)
        model.getCityScore(city)
    }

    override fun updateDataView(result: Any?, error: String?) {
        if (result != null && result is String) {
            score = result
            model.getCityGeocode()
        } else if (result != null && result is LocationObject) {
            latitude = result.latitude
            longitude = result.longitude
        }
        view.updateView()
        view.showProgressBar(View.GONE)
    }

    override fun getCityName(): String {
        return cityName
    }

    override fun getScore(): String {
        return score
    }

    override fun getLatitude(): Double {
        return latitude
    }

    override fun getLongitude(): Double {
        return longitude
    }

}