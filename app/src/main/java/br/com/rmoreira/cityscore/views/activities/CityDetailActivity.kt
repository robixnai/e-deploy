package br.com.rmoreira.cityscore.views.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import br.com.rmoreira.cityscore.R
import br.com.rmoreira.cityscore.contracts.CityScoreMvp
import br.com.rmoreira.cityscore.presenters.CityDetailPresenter
import com.google.android.gms.maps.GoogleMap
import kotlinx.android.synthetic.main.activity_city_detail.*
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import android.location.Geocoder
import android.util.Log
import java.util.*

class CityDetailActivity : AppCompatActivity(), CityScoreMvp.CityDetailViewImpl {

    companion object {
        val CITY = "city"
    }

    private lateinit var presenter: CityDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_detail)

        val extras = intent.extras
        presenter = CityDetailPresenter(this)
        presenter.getCityScore(extras)
    }

    override fun updateView() {
        val cityName = presenter.getCityName()

        val cityScore = resources.getString(R.string.txt_city_score, cityName, presenter.getScore())
        textViewCityScore.setHint(cityScore)

        addMap(cityName)
    }

    override fun showProgressBar(visibility: Int) {
        progressBarCityDetail.visibility = visibility
    }

    private fun addMap(cityName: String) {
        val latLng = LatLng(presenter.getLatitude(), presenter.getLongitude())
        val geocoder = Geocoder(this, Locale.getDefault())
        val location = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
        Log.d("TESTE", "location: " + location.size)
        if (location.size > 0) {
            val mapFragment = fragmentManager.findFragmentById(R.id.mapViewCityScore) as MapFragment

            mapFragment.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(map: GoogleMap) {
                    val cameraPosition = CameraPosition.Builder()
                            .target(latLng)
                            .zoom(12.4f)
                            .tilt(90f)
                            .build()

                    map.addMarker(MarkerOptions()
                            .position(latLng)
                            .title(cityName))
                    map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    map.setBuildingsEnabled(true)
                }
            })
        }
    }

}
