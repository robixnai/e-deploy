package br.com.rmoreira.cityscore.objects

import com.google.gson.annotations.SerializedName

/**
 * Created by robsonmoreira on 15/10/17.
 */
class GeometryObject(@SerializedName("location") var location: LocationObject)