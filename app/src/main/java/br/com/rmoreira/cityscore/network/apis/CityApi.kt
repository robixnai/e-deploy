package br.com.rmoreira.cityscore.network.apis

import br.com.rmoreira.cityscore.objects.CityObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by robsonmoreira on 15/10/17.
 */
interface CityApi {

    companion object {
        val BASE: String get() = "http://wsteste.devedp.com.br/Master/CidadeServico.svc/rest/"
    }

    @GET("BuscaTodasCidades")
    fun getCities(): Call<ArrayList<CityObject>>

    @POST("BuscaPontos")
    fun cityScore(@Body city: CityObject): Call<String>

}